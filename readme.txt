Information about the package should be fed into the <description> in the tab <general>, after <identifier> and  
<title>

The "Worksheet" should be added as a <file> under the <file> of the original content (line 98)
The <type> of "worksheet" should be "BCWorksheet" (line 101)


The "Teacher's notes" should be added as a <file> under "worksheet" (line 105)
The <type> of "Teacher's notes" should be "BCTeachernotes" (line 108)

NOTE: if the textual value carries any special character or style elements (numbers/symbols), they should be added  
through the CDATA, with the corresponding values

COVER IMAGES SHOULD BE IN TEH SIZE OF 158X231 only

CONTENT SHULD BE UPLOADED TO "clp-uat-repo/Content/UAT/qbslearning/" (This must include the worksheet and teacher  
notes)

For example if the title to be uploaded is "Wheels", then upload the content under the directory "/qbslearning/" as  
"/wheels/" and the teacher notes and worksheet under "/wheels/" in a directory named "/resources/"

the URL to the content/resources should be then "https://content.connect-uat.co.uk/Content/UAT/qbslearning/Bigcat/Wheels/index.html" for the content (e-book)

And for the teacher note/workseet "https://content.connect-uat.co.uk/Content/UAT/qbslearning/Bigcat/Wheels/resources/teachersnotes.docx"
